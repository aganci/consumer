﻿using Consumer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Support
{
    class TestRequest : IRequest
    {
        public bool processed;

        public TestRequest(Priority priority)
        {
            this.Priority = priority;
        }

        public Priority Priority { get; private set; }

        public void Process()
        {
            processed = true;
        }
    }
}
