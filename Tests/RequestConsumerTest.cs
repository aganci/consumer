﻿using Consumer;
using NuGet.Frameworks;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Tests.Support;

namespace Tests
{
    public class RequestConsumerTest
    {
        private FifoQueue queue;
        private RequestConsumer consumer;

        [SetUp]
        public void SetUp()
        {
            queue = new FifoQueue();
            consumer = new RequestConsumer(queue);
        }

        [Test]
        public void ShouldProcessRequest()
        {
            var request = PushRequest(Priority.High);

            consumer.ProcessRequest();

            Assert.That(request.processed, Is.True);
        }

        [Test]
        public void ShouldProcessHighPriorityRequestsFirst()
        {
            var lowPriority = PushRequest(Priority.Low);
            var highPriority = PushRequest(Priority.High);

            consumer.ProcessRequest();

            Assert.That(highPriority.processed, Is.True);
            Assert.That(lowPriority.processed, Is.False);
        }

        [Test]
        public void ShouldProcessHighPriorityFirstGivenMultipleLowPriority()
        {
            var firstLow = PushRequest(Priority.Low);
            var secondLow = PushRequest(Priority.Low);
            var highPriority = PushRequest(Priority.High);

            consumer.ProcessRequest();
            Assert.That(highPriority.processed, Is.True);
            Assert.That(firstLow.processed, Is.False);
            Assert.That(secondLow.processed, Is.False);

            consumer.ProcessRequest();
            Assert.That(firstLow.processed, Is.True);
            Assert.That(secondLow.processed, Is.False);

            consumer.ProcessRequest();
            Assert.That(secondLow.processed, Is.True);
        }

        [Test]
        public void ShouldProcessMaximumFourHighPriorityInARow()
        {
            for(int i = 0; i < RequestConsumer.STARVATION_THRESHOLD + 1; i++)
            {
                PushRequest(Priority.High);
            }
            var lowPriority = PushRequest(Priority.Low);

            for (int i = 0; i < RequestConsumer.STARVATION_THRESHOLD; i++)
            {
                consumer.ProcessRequest();
                Assert.That(lowPriority.processed, Is.False);
            }

            consumer.ProcessRequest();
            Assert.That(lowPriority.processed, Is.True);
        }

        [Test]
        public void ShouldWaitForFourHighPriorityRequestsBeforeProcessALowPriority()
        {
            var lowPriority = PushRequest(Priority.Low);
            for (int i = 0; i < RequestConsumer.STARVATION_THRESHOLD + 1; i++)
            {
                PushRequest(Priority.High);
            }

            for (int i = 0; i < RequestConsumer.STARVATION_THRESHOLD; i++)
            {
                consumer.ProcessRequest();
                Assert.That(lowPriority.processed, Is.False);
            }

            consumer.ProcessRequest();
            Assert.That(lowPriority.processed, Is.True);
        }

        [Test]
        public void ShouldOvercomeTheStarvationThresholdWithoutLowPriorityRequest()
        {
            var requests = new List<TestRequest>();
            for (int i = 0; i < RequestConsumer.STARVATION_THRESHOLD + 1; i++)
            {
                requests.Add(PushRequest(Priority.High));
            }

            for (int i = 0; i < RequestConsumer.STARVATION_THRESHOLD + 1; i++)
            {
                consumer.ProcessRequest();
            }

            Assert.That(requests.TrueForAll(r => r.processed), Is.True);
        }

        [Test]
        public void ShouldProcessLowPriorityGivenNoAvailableHighPriorityRequest()
        {
            var request = PushRequest(Priority.Low);

            consumer.ProcessRequest();

            Assert.That(request.processed, Is.True);
        }

        [Test]
        public void ShouldNotProcessGivenNoRequests()
        {
            Assert.DoesNotThrow(() => { consumer.ProcessRequest(); });
        }

        TestRequest PushRequest(Priority priority)
        {
            var result = new TestRequest(priority);
            queue.Push(result);
            return result;
        }
    }
}
