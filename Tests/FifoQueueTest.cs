using Consumer;
using NUnit.Framework;
using Tests.Support;

namespace Tests
{
    public class FifoQueueTest
    {
        FifoQueue queue;

        [SetUp]
        public void Setup()
        {
            queue = new FifoQueue();
        }

        [Test]
        public void ShouldPopElements()
        {
            var firstRequest = new TestRequest(Priority.Low);
            queue.Push(firstRequest);
            var secondRequest = new TestRequest(Priority.High);
            queue.Push(secondRequest);

            Assert.That(queue.Pop(), Is.EqualTo(firstRequest));
            Assert.That(queue.Pop(), Is.EqualTo(secondRequest));
        }

        [Test]
        public void ShouldOrderByPriority()
        {
            var highPriority = new TestRequest(Priority.High);
            queue.Push(highPriority);
            var lowPriority = new TestRequest(Priority.Low);
            queue.Push(lowPriority);

            queue.SortByPriority();

            Assert.That(queue.Pop(), Is.EqualTo(lowPriority));
            Assert.That(queue.Pop(), Is.EqualTo(highPriority));
        }

        [Test]
        public void ShouldReturnNullGivenNoRequests()
        {
            Assert.That(queue.Pop(), Is.Null);
        }
    }
}