﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Consumer
{
    public class RequestConsumer
    {
        public const int STARVATION_THRESHOLD = 4;

        private IQueue queue;
        private FifoQueue lowPriorityQueue = new FifoQueue();
        private int highPriorityInARow = 0;

        public RequestConsumer(IQueue queue)
        {
            this.queue = queue;
        }

        public void ProcessRequest()
        {
            IRequest highPriorityRequest = DequeueLowPriorityRequests();

            if (highPriorityRequest == null)
            {
                ProcessLowPriorityRequest();
            }
            else
            {
                ProcessHighPriorityRequest(highPriorityRequest);
            }
        }

        private void ProcessLowPriorityRequest()
        {
            IRequest request = lowPriorityQueue.Pop();
            if (request == null)
            {
                return;
            }
            request.Process();
            highPriorityInARow = 0;
        }

        private void ProcessHighPriorityRequest(IRequest highPriorityRequest)
        {
            if (highPriorityInARow < STARVATION_THRESHOLD)
            {
                highPriorityRequest.Process();
                highPriorityInARow++;
                return;
            }

            var lowPriority = GetFirstAvailableLowPriorityRequest();
            if (lowPriority != null)
            {
                lowPriority.Process();
                highPriorityInARow = 0;
            }
            else
            {
                highPriorityRequest.Process();
                highPriorityInARow++;
            }
        }

        private IRequest GetFirstAvailableLowPriorityRequest()
        {
            var lowPriority = lowPriorityQueue.Pop();
            if (lowPriority != null)
            {
                return lowPriority;
            }
            queue.SortByPriority();
            return queue.Pop();
        }

        private IRequest DequeueLowPriorityRequests()
        {
            IRequest request;
            do
            {
                request = this.queue.Pop();
                if (request != null && request.Priority == Priority.Low)
                {
                    lowPriorityQueue.Push(request);
                }
            }
            while (request != null && request.Priority == Priority.Low);
            return request;
        }
    }
}
