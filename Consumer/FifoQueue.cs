﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Consumer
{
    public class FifoQueue : IQueue
    {
        List<IRequest> requests = new List<IRequest>();

        public IRequest Pop()
        {
            if (requests.Count == 0)
            {
                return null;
            }

            var result = requests[0];
            requests.RemoveAt(0);
            return result;
        }

        public void Push(IRequest request)
        {
            this.requests.Add(request);
        }

        public void SortByPriority()
        {
            requests = requests.OrderBy(r => r.Priority).ToList();
        }
    }
}
