﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Consumer
{
    /// <summary>
    /// Represents a FIFO queue
    /// </summary>
    public interface IQueue
    {
        /// <summary>
        /// Pushes the given request into the queue
        /// </summary>
        void Push(IRequest request);
        /// <summary>
        /// Pops the first request from the queue
        /// </summary>
        IRequest Pop();
        /// <summary>
        /// Sorts the requests contained in the queue by priority ascending.
        /// That is, low priority requests first, then high priority ones.
        /// </summary>
        void SortByPriority();
    }
    
    public interface IRequest
    {
        Priority Priority { get; }
        void Process();
    }
    
    public enum Priority
    {
        Low,
        High
    }
}
